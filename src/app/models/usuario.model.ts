export class UsuarioModel {
   rut!: string;
   nombre!: string;
   celular!: string;
   email!: string;
   banco!: string;
   tipo_cuenta!: string;
   nro_cuenta!: string;
}