import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UsuarioModel } from "src/app/models/usuario.model";
import { validateRut, formatRut, RutFormat } from '@fdograph/rut-utilities';
import { TransacionService } from '../../services/transacion.service';
import Swal from 'sweetalert2';
import { map } from 'rxjs';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  [x: string]: any;
 
  // datos = []
  datos: any;
  usuario: UsuarioModel = new UsuarioModel;
  messageFailure: string | undefined;
  recordarUser: boolean = false;
  bancos = "https://bast.dev/api/banks.php"; 
  
  constructor(
    private service: TransacionService, 
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.usuario = new UsuarioModel();
    this.datos = this.http.get(`${ this.bancos }/`)
    
    this.service.buscador().subscribe((f) => {
     
      localStorage.setItem('usuario', JSON.stringify(f))
      console.log(JSON.stringify( f))
  });
  }

  registrar(form: NgForm) {
    if (form.invalid) {
      return;
    }

  

    // mostrar loading sweetAlert
    // Swal.showLoading();

    this.service.registrarUsuario(this.usuario)
    .subscribe( (resp) => {
      console.log("Usuario Creado correctamente", resp);
      
      
      // cerrar Modal sweetAlert
      // Swal.close();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Registro exitoso',
        showConfirmButton: false,
        timer: 1300
      })



      
      this.router.navigateByUrl('/home');
      
    }, error => {
      
    
      this.messageFailure = 'El usuario ya existe.';
      this.limpiarImputs();
    });
  }


  listBank() { 

    this.service.servicioBancos().subscribe((res) => { 
      console.log('lista de bancos', res);
      
      this['requests']=res
    }, error => {

    })
  }


  limpiarImputs(){
    this.usuario = new UsuarioModel();
  }

}
