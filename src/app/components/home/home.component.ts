import { Component, OnInit, Type } from '@angular/core';
import { map, Subject} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { TransacionService } from '../../services/transacion.service';
import { Usuario } from '../../intefaces/usuario.interface';
import axios, { Axios } from 'axios';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  
  listFiltered: any;

  
  // api = axios.get('http://localhost:33000/usuario').then(e => {
  //  return e.data
  // });

  api = axios.get('https://api-rest-banco-jx65pltqmq-uc.a.run.app/usuario').then(e => {
    return e.data
   });

  result = localStorage.getItem('usuario');


  searchTerm$ = new Subject<string>();
  
  
  constructor(
    public service : TransacionService,
    private http: HttpClient
  ) { }
  

  
  data: any = this.service.buscador();
 
  ngOnInit(): void {
    
    this.listFiltered = this.service.buscador().subscribe((r) => {
      console.log('dsfsdfsdfsd', r)
      return r;
    });
 
    this.service.buscador().subscribe((f) => {
   

      console.log(JSON.stringify( f))
    });
  }

}
