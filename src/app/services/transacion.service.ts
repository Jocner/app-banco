import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsuarioModel } from '../models/usuario.model';
import { Usuario } from '../intefaces/usuario.interface'
import { tap, map, catchError, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransacionService {

  private api = "https://api-rest-banco-jx65pltqmq-uc.a.run.app/usuario";

  private bancos = "https://bast.dev/api/banks.php"; 

  constructor(private http: HttpClient) { }

  registrarUsuario(usuario: UsuarioModel){
    const data = {
      ...usuario
    }

    return this.http.post(`${ this.api }/`, data);
  }

  servicioBancos() {

    const bank = this.http.get(`${ this.bancos }/`)
    
    return bank;
    
  }

  // buscador(): Observable<UsuarioModel[]> {

  //   const data = this.http.get<UsuarioModel[]>(`${ this.api }/`);

  //   return data;

  // }

  //  buscador():Observable<Usuario> {

  //   let data = this.http.get<Usuario>(`${ this.api }/`);
    
    
  //   return data;

  // }

  buscador() {

    let data = this.http.get(`${ this.api }/`);
    
    
    return data;

  }

  // buscador() {

  //   const data = this.http.get(`${ this.api }/`).pipe( 
  //     tap((res: any) => {
        
  //       // localStorage.setItem('data', res );
  //       console.log("historia",res )
  //       sessionStorage.setItem('transaciones', JSON.stringify(res) );              

  //     }
  //      ));


  //   return data;

  // }
}
